const { promisify } = require('util');
const { glob } = require('glob');
const pGlob = promisify(glob);
const Logger = require('../Logger');
const { PermissionsBitField } = require('discord.js');

module.exports = async client => {
  (await pGlob(`${process.cwd()}/commands/*/*.js`)).map(async cmdFile => {

    const cmd = require(cmdFile);

    if (!cmd.name) return Logger.warn(`Command not load: No name ↓\nFile -> ${cmdFile}`);

    if (!cmd.description && cmd.type != 2) return Logger.warn(`Command not load: No description ↓\nFile -> ${cmdFile}`);

    if (!cmd.category) return Logger.warn(`Command not load: No category ↓\nFile -> ${cmdFile}`)

    if (!cmd.permissions) return Logger.warn(`Command not load: No permission ↓\nFile -> ${cmdFile}`)

    if (cmd.ownerOnly == undefined) return Logger.warn(`Command not load: Specify if ownerOnly ↓\nFile -> ${cmdFile}`)

    if (!cmd.usage) return Logger.warn(`Command not load: Add usage to your command ↓\nFile -> ${cmdFile}`)

    if (!cmd.examples) return Logger.warn(`Command not load: Add examples to your command ↓\nFile -> ${cmdFile}`)

    cmd.permissions.forEach(permission => {
      if (!permissionList.includes(permission)) {
        return Logger.typo(`Command not load: Permission's name not correct for ${permission} ↓\nFile -> ${cmdFile}`);
      }
    })

    client.commands.set(cmd.name, cmd);

    Logger.command(`- ${cmd.name}`);
  });
};

const permissionList = [PermissionsBitField.Flags.AddReactions, PermissionsBitField.Flags.Administrator, PermissionsBitField.Flags.AttachFiles, PermissionsBitField.Flags.BanMembers, PermissionsBitField.Flags.ChangeNickname, PermissionsBitField.Flags.Connect, PermissionsBitField.Flags.CreateInstantInvite, PermissionsBitField.Flags.CreatePrivateThreads, PermissionsBitField.Flags.CreatePublicThreads, PermissionsBitField.Flags.DeafenMembers, PermissionsBitField.Flags.EmbedLinks, PermissionsBitField.Flags.KickMembers, PermissionsBitField.Flags.ManageChannels, PermissionsBitField.Flags.ManageEmojisAndStickers, PermissionsBitField.Flags.ManageEvents, PermissionsBitField.Flags.ManageGuild, PermissionsBitField.Flags.ManageMessages, PermissionsBitField.Flags.ManageNicknames, PermissionsBitField.Flags.ManageRoles, PermissionsBitField.Flags.ManageThreads, PermissionsBitField.Flags.ManageWebhooks, PermissionsBitField.Flags.MentionEveryone, PermissionsBitField.Flags.ModerateMembers, PermissionsBitField.Flags.MoveMembers, PermissionsBitField.Flags.MuteMembers, PermissionsBitField.Flags.PrioritySpeaker, PermissionsBitField.Flags.ReadMessageHistory, PermissionsBitField.Flags.RequestToSpeak, PermissionsBitField.Flags.SendMessages, PermissionsBitField.Flags.SendMessagesInThreads, PermissionsBitField.Flags.SendTTSMessages, PermissionsBitField.Flags.Speak, PermissionsBitField.Flags.Stream, PermissionsBitField.Flags.UseApplicationCommands, PermissionsBitField.Flags.UseEmbeddedActivities, PermissionsBitField.Flags.UseExternalEmojis, PermissionsBitField.Flags.UseExternalStickers, PermissionsBitField.Flags.UseVAD, PermissionsBitField.Flags.ViewAuditLog, PermissionsBitField.Flags.ViewChannel, PermissionsBitField.Flags.ViewGuildInsights];