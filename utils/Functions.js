const { Guild } = require('../models');
const Logger = require('./Logger');
const { Colors } = require('discord.js');
const fetch = require('node-fetch');

module.exports = client => {
  client.getGuild = async guild => {
    const guildData = await Guild.findOne({ id: guild.id });
    return guildData;
  };

  client.createGuild = async guild => {
    const criminal = await client.getRole(guild, 'CRIMINAL');
    const stars = await client.getRole(guild, 'STARS');
    const acceptedRules = await client.getRole(guild, 'ACCEPTED RULES');
    if (!criminal) {
      guild.roles.create(
        {
          name: 'CRIMINAL',
          color: Colors.Red
        }
      )
    }
    if (!stars) {
      guild.roles.create(
        {
          name: 'STARS',
          color: Colors.Yellow
        }
      )
    }
    if (!acceptedRules) {
      guild.roles.create(
        {
          name: 'ACCEPTED RULES',
          color: Colors.Green
        }
      )
    }

    let aiucLogs = await client.getLogChannel(guild);
    if (!aiucLogs) {
      aiucLogs = await guild.channels.create('aiuc-logs', { reason: 'AIUC BOT need this channel for logs' });
    }

    const createGuild = new Guild({ id: guild.id, logChannel: aiucLogs.id });
    createGuild.save().then(g => Logger.client(`New server (${g.id})`));
  };

  client.updateGuild = async (guild, settings) => {
    let guildData = await client.getGuild(guild);
    if (typeof guildData != 'object') guildData = {};
    for (const key in settings) {
      if (guildData[key] != settings[key]) guildData[key] = settings[key]
    }
    return guildData.updateOne(settings);
  };

  client.getRole = async (guild, role) => {
    const gettingRole = await guild.roles.cache.find(guildRole => guildRole.name === role);
    return gettingRole;
  };

  client.getLogChannel = async guild => {
    const gettingLog = await guild.channels.cache.find(logChannel => logChannel.name === 'aiuc-logs');
    return gettingLog;
  };

  client.getRandomGif = async (searchTerm, limit) => {
    var apiKey = process.env.TENOR_API_KEY;
    var searchUrl = "https://g.tenor.com/v1/random?q=" + searchTerm + "&key=" + apiKey + "&limit=" + limit;
    const response = await fetch(searchUrl);
    const data = await response.json();
    return data;
  };
}