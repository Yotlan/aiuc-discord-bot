const { Client, Collection, GatewayIntentBits } = require('discord.js');
const { DisTube } = require('distube');
const { YtDlpPlugin } = require('@distube/yt-dlp');
const { Configuration, OpenAIApi } = require('openai');
const client = new Client(
  {
    intents: [
      GatewayIntentBits.Guilds,
      GatewayIntentBits.GuildMembers,
      GatewayIntentBits.GuildMessages,
      GatewayIntentBits.GuildVoiceStates,
      GatewayIntentBits.MessageContent
    ]
  }
);
const mongoose = require('mongoose');
const Logger = require('./utils/Logger');
const express = require('express');

['commands', 'buttons', 'selects'].forEach(x => client[x] = new Collection());
['CommandUtil', 'EventUtil', 'ButtonUtil', 'SelectUtil'].forEach(handler => { require(`./utils/handlers/${handler}`)(client) });
require('./utils/Functions')(client);

mongoose.set('strictQuery', true);

process.on('exit', code => { Logger.client(`Process shutdown with the following code: ${code}`) });
process.on('uncaughtException', (err, origin) => {
  Logger.error(`UNCAUGHT_EXCEPTION: ${err}`);
  console.error(`Origin: ${origin}`);
});
process.on('unhandledRejection', (reason, promise) => {
  Logger.warn(`UNHANDLED_EXCEPTION: ${reason}`);
  console.log(promise);
});
process.on('warning', (...args) => Logger.warn(...args));

mongoose.connect(process.env.DATABASE_URI, {
  autoIndex: false,
  maxPoolSize: 10,
  serverSelectionTimeoutMS: 5000,
  socketTimeoutMS: 45000,
  family: 4
}).then(() => { Logger.client('Connected to the database!') })
  .catch(err => { Logger.error(err) });

client.distube = new DisTube(client, {
  leaveOnStop: false,
  emitNewSongOnly: true,
  emitAddSongWhenCreatingQueue: false,
  emitAddListWhenCreatingQueue: false,
  plugins: [
    new YtDlpPlugin()
  ]
});

const configuration = new Configuration({
  organization: process.env.CHATGPT_ORG_ID,
  apiKey: process.env.CHATGPT_API_KEY
});

client.openai = new OpenAIApi(configuration);

client.login(process.env.DISCORD_TOKEN);
const app = express();
const port = 3000;
app.get('/', (req, res) => {
  res.send('Hello World!')
});
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});