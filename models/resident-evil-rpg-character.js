const mongoose = require('mongoose');

const residentEvilRpgCharacterSchema = mongoose.Schema({
  id: String,
  member: String,
  lastname: String,
  firstname: String,
  surname: String,
  age: String,
  sexe: String,
  family: String,
  nationality: String,
  sexualorientation: String,
  politicalparty: String,
  religion: String,
  history: String,
  other: String,
  picture: String
});

module.exports = mongoose.model('ResidentEvilRPGCharacter', residentEvilRpgCharacterSchema);