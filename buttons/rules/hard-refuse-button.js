module.exports = {
  name: 'hard-refuse-button',
  async runInteraction(client, interaction) {

    try {
      await interaction.member.send('You not accept the rules, so I kick you!');
    } catch (e) {
      await interaction.reply(`The member ${interaction.member.displayName} not accept the rule, so I kick him!`);
    }

    await interaction.member.kick('He not accept the rules!');
  }
}