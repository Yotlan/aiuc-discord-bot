module.exports = {
  name: 'soft-refuse-button',
  async runInteraction(client, interaction) {
    await interaction.reply(`The member ${interaction.member.displayName} not accept the rule!`);
  }
}