module.exports = {
  name: 'moderate-refuse-button',
  async runInteraction(client, interaction) {

    try {
      await interaction.member.send('You not accept the rules, please accept the rules to access to the whole content of the server!');
    } catch (e) {
      await interaction.reply(`The member ${interaction.member.displayName} not accept the rule, I try to send him a message, but I cannot do it!`);
    }
  }
}