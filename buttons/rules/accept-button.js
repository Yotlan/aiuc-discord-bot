module.exports = {
  name: 'accept-button',
  async runInteraction(client, interaction) {
    const role = await client.getRole(interaction.guild, 'ACCEPTED RULES');
    await interaction.member.roles.add(`${role.id}`);
    await interaction.reply({ content: 'You accept the rules! You can know access to the server!' });
  }
}