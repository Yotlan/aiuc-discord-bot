module.exports = {
  name: 'back',
  async runInteraction(client, interaction) {
    if (!interaction.member.voice.channel)
      return interaction.reply({
        content: `❌ You need to be in a voice channel to do that!`,
        ephemeral: true,
      });

    if (
      client.voice.channel &&
      interaction.member.voice.channelId !==
      client.voice.channelId
    )
      return interaction.reply({
        content: `❌ You need to be in the same voice channel as me to do that`,
        ephemeral: true,
      });

    const queue = client.distube.getQueue(interaction.guildId);

    if (!queue || !queue.playing)
      return interaction.reply({
        content: `❌ There is no music playing in this guild !`,
        ephemeral: true,
      });
    if (queue) {
      if (queue.previousSongs.length > 0) {
        const backed = queue.previous();
        return await interaction.reply(
          backed
            ? `⏮️ Now Playing the previous track from your queue!`
            : `❌ Failed to do that!`
        );
      } else {
        return interaction.reply({
          content: `❌ There is no previous track in your queue!`,
          ephemeral: true,
        });
      }
    }
  }
};