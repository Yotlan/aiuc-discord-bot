module.exports = {
  name: 'skip',
  async runInteraction(client, interaction) {
    if (!interaction.member.voice.channel)
      return interaction.reply({
        content: `❌ You need to be in a voice channel to do that!`,
        ephemeral: true,
      });

    if (
      client.voice.channel &&
      interaction.member.voice.channelId !==
      client.voice.channelId
    )
      return interaction.reply({
        content: `❌ You need to be in the same voice channel as me to do that`,
        ephemeral: true,
      });

    const queue = client.distube.getQueue(interaction.guildId);
    if (!queue || !queue.playing)
      return interaction.reply({
        content: "❌ No music is playing in this guild",
        ephemeral: true,
      });

    const currentTrack = queue.songs[0].name;
    if (queue.songs.length < 2) {
      return interaction.reply(`❌ Only 1 song in your queue`);
    }
    const success = queue.skip();
    return interaction.reply({
      content: success
        ? ` ⏭ Skipped **${currentTrack}**!`
        : "❌ Failed to do that!",
    });
  },
};