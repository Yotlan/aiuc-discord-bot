module.exports = {
  name: 'pause',
  async runInteraction(client, interaction) {
    if (!interaction.member.voice.channel)
      return interaction.reply({
        content: `❌ You need to be in a voice channel to do this!`,
        ephemeral: true,
      });

    if (
      client.voice.channel &&
      interaction.member.voice.channelId !==
      client.voice.channelId
    )
      return interaction.reply({
        content: `❌ You need to be in the same voice channel as me to do that`,
        ephemeral: true,
      });

    const queue = client.distube.getQueue(interaction.guildId);

    if (!queue || !queue.playing)
      return interaction.reply({
        content: `❌ There is no music playing  in this guild !`,
        ephemeral: true,
      });

    if (queue) {
      let y = queue.pause();
      return interaction.reply(
        y ? ` ⏸ Paused !` : `❌ Failed to pause `
      );
    }
  },
};