module.exports = {
  name: 'mute',
  async runInteraction(client, interaction) {
    if (!interaction.member.voice.channel)
      return interaction.reply({
        content: `❌ You're not in a voice channel !`,
        ephemeral: true,
      });

    if (
      client.voice.channel &&
      interaction.member.voice.channelId !==
      client.voice.channelId
    )
      return interaction.reply({
        content: `❌ You are not in the same voice channel !`,
        ephemeral: true,
      });

    const queue = client.distube.getQueue(interaction.guildId);

    if (!queue || !queue.playing)
      return interaction.reply({
        content: `❌ There is no music playing in this guild !`,
        ephemeral: true,
      });

    if (queue) {
      queue.setVolume(0);
      return interaction.reply(`🔇 Muted myself!`);
    }
  }
};