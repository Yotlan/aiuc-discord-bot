module.exports = {
  name: 'volume-down',
  async runInteraction(client, interaction) {
    if (!interaction.member.voice.channel)
      return interaction.reply({
        content: `❌ You're not in a voice channel !`,
        ephemeral: true,
      });

    if (
      client.voice.channel &&
      interaction.member.voice.channelId !==
      client.voice.channelId
    )
      return interaction.reply({
        content: `❌ You are not in the same voice channel !`,
        ephemeral: true,
      });

    const queue = client.distube.getQueue(interaction.guildId);

    if (!queue || !queue.playing)
      return interaction.reply({
        content: `❌ There is no music playing  in this guild !`,
        ephemeral: true,
      });

    if (queue) {
      let volume = queue.volume;
      if (volume.value <= 0) {
        return interaction.reply(
          `❌ Your volume cannot be decreased as it's 0 already`
        );
      }
      volume = volume - 2;
      queue.setVolume(volume);

      return interaction.reply(
        `🔉 Volume decreased to ${volume}`
      );
    }
  },
};