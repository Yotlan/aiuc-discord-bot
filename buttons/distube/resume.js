module.exports = {
  name: 'resume',
  async runInteraction(client, interaction) {
    if (!interaction.member.voice.channel)
      return interaction.reply({
        content: `❌ You need to be in a voice channel to do this!`,
        ephemeral: true,
      });

    if (
      client.voice.channel &&
      interaction.member.voice.channelId !==
      client.voice.channelId
    )
      return interaction.reply({
        content: `❌ You need to be in the same voice channel as me to do that`,
        ephemeral: true,
      });

    const queue = client.distube.getQueue(interaction.guildId);

    if ((!queue || !queue.playing) && !queue.paused)
      return interaction.reply({
        content: `❌ There is no music playing  in this guild !`,
        ephemeral: true,
      });

    if (queue) {
      let x = await queue.resume();
      return await interaction.reply(
        x ? ` ▶ Resumed !` : `❌ Failed to resume`
      );
    }
  },
};