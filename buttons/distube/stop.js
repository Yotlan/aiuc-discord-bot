module.exports = {
  name: 'stop',
  async runInteraction(client, interaction) {
    if (!interaction.member.voice.channel)
      return interaction.reply({
        content: `❌ You need to be in a voice channel to do that!`,
        ephemeral: true,
      });

    if (
      client.voice.channel &&
      interaction.member.voice.channelId !==
      client.voice.channel.id
    )
      return interaction.reply({
        content: `❌ You need to be in the same voice channel as me to do that`,
        ephemeral: true,
      });
    const queue = client.distube.getQueue(interaction.guildId);
    if (!queue || !queue.playing)
      return interaction.reply({
        content: "❌ No music is being played!",
      });

    queue.stop();
    client.distube.voices.leave(interaction.guildId);
    return interaction.reply("⏹️ Successfully Stopped the music");
  },
};