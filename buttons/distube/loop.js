module.exports = {
  name: 'loop',
  async runInteraction(client, interaction) {
    if (!interaction.member.voice.channel)
      return interaction.reply({
        content: `❌ You need to be in a voice channel to do this!`,
        ephemeral: true,
      });

    if (
      client.voice.channel &&
      interaction.member.voice.channelId !==
      client.voice.channelId
    )
      return interaction.reply({
        content: `❌ You need to be in the same voice channel as me to do that`,
        ephemeral: true,
      });
    const queue = client.distube.getQueue(interaction.guildId);
    if (!queue || !queue.playing) {
      return interaction.reply({
        content: `❌ There is nothing playing right now!`,
        ephemeral: true,
      });
    }
    const repeat = queue.repeatMode
    if (repeat === 0) {
      const x = await queue.setRepeatMode(1);
      return interaction.reply(`🔂 Successfully enabled loop Song`);
    }
    if (repeat === 1) {
      const x = await queue.setRepeatMode(2);
      return interaction.reply(`🔁 Successfully enabled loop Playlist`);
    }
    if (repeat === 2) {
      const x = await queue.setRepeatMode(0);
      return interaction.reply(`Successfully disabled loop`);
    }
  }
};