const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'image',
  category: 'openai',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'image [message]',
  examples: ['image STARS member', 'chat BSSA member'],
  description: 'Tell to AIUC to generate an image',
  options: [
    {
      name: 'message',
      description: 'Enter whatever you want to tell to AIUC bot generate an image',
      type: ApplicationCommandOptionType.String,
      required: true,
    }
  ],
  async runInteraction(client, interaction) {
    const message = interaction.options.getString('message');
      
    const image = await interaction.reply({ content: 'Waiting...', fetchReply: true });

    try {
      const imagereply = await client.openai.createImage({
        prompt: message,
        n: 1,
        size: "1024x1024"
      });
      
      const attachment = new AttachmentBuilder(imagereply.data.data[0].url, { name: 'dall-e-image.png' });
      
      interaction.editReply({ content: ' ', files: [attachment] });
    } catch(e) {
      console.log(e);
      return await interaction.editReply({ content: `Request failed with status code **${e.response.status}**` })
    }
  }
}