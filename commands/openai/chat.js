const { ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'chat',
  category: 'openai',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'chat [message]',
  examples: ['chat How are your today?', 'chat Generate me a backstory of a new character in resident evil first trilogy'],
  description: 'Interact with AIUC bot',
  options: [
    {
      name: 'message',
      description: 'Enter whatever you want to interact with AIUC bot',
      type: ApplicationCommandOptionType.String,
      required: true,
    }
  ],
  async runInteraction(client, interaction) {
    const message = interaction.options.getString('message');
      
    const chat = await interaction.reply({ content: 'Waiting...', fetchReply: true });

    try {
      const chatreply = await client.openai.createCompletion({
        model: 'text-davinci-003',
        prompt: message,
        temperature: 0,
        max_tokens: 2048
      })
      
      interaction.editReply({ content: `${chatreply.data.choices[0].text}` });
    } catch(e) {
      return await interaction.editReply({ content: `Request failed with status code **${e.response.status}**` })
    }
  }
}