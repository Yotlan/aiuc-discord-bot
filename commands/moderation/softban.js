const { ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'softban',
  category: 'moderation',
  permissions: [PermissionsBitField.Flags.ModerateMembers],
  ownerOnly: false,
  usage: 'softban [@member] [duration] [reason]',
  examples: ['softban @Yotlan LE CROM 4 minutes It\'s not cool to be bad!'],
  description: 'Softban a user with reason for a given duration',
  options: [
    {
      name: 'target',
      description: 'User to softban',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
    {
      name: 'duration',
      description: 'Duration of the softban',
      type: ApplicationCommandOptionType.Number,
      minValue: 1,
      maxValue: 7,
      required: true,
    },
    {
      name: 'reason',
      description: 'Reason of the softban',
      type: ApplicationCommandOptionType.String,
      required: true,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');
    const duration = interaction.options.getNumber('duration');
    const reason = interaction.options.getString('reason');

    if (!target.bannable) return interaction.reply('This member cannot be ban by the bot!');

    target.ban({ days: duration, reason: reason });
    interaction.reply(`The member ${target} was ban for ${duration} days because ${reason}!`);
  }
}