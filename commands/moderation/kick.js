const { ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'kick',
  category: 'moderation',
  permissions: [PermissionsBitField.Flags.KickMembers],
  ownerOnly: false,
  usage: 'kick [@member] [reason]',
  examples: ['kick @Yotlan LE CROM It\'s not cool to be bad!'],
  description: 'Kick a user with reason',
  options: [
    {
      name: 'target',
      description: 'User to kick',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
    {
      name: 'reason',
      description: 'Reason of the kick',
      type: ApplicationCommandOptionType.String,
      required: true,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');
    const reason = interaction.options.getString('reason');

    if (!target.kickable) return interaction.reply('This member cannot be kick by the bot!');

    target.kick(reason);
    interaction.reply(`The member ${target} was kick because ${reason}!`);
  }
}