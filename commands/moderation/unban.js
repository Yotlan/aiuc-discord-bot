const { ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'unban',
  category: 'moderation',
  permissions: [PermissionsBitField.Flags.BanMembers],
  ownerOnly: false,
  usage: 'unban [@member]',
  examples: ['unban @Yotlan LE CROM'],
  description: 'Unban a user',
  options: [
    {
      name: 'target',
      description: 'User to unban',
      type: ApplicationCommandOptionType.String,
      required: true,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getString('target');

    const banList = await interaction.guild.bans.fetch();

    const bannedUser = banList.find(guildBan => guildBan.user.username === target).user;

    if (!bannedUser) return interaction.reply('This member cannot be unban by the bot!');

    interaction.guild.bans.remove(bannedUser.id);
    interaction.reply(`The member ${target} was unban!`);
  }
}