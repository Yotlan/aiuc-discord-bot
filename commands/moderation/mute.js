const { ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const ms = require('ms');

module.exports = {
  name: 'mute',
  category: 'moderation',
  permissions: [PermissionsBitField.Flags.ModerateMembers],
  ownerOnly: false,
  usage: 'mute [@member] [duration] [reason]',
  examples: ['mute @Yotlan LE CROM 4 minutes It\'s not cool to be bad!'],
  description: 'Mute a user with reason for a given duration',
  options: [
    {
      name: 'target',
      description: 'User to mute',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
    {
      name: 'duration',
      description: 'Duration of the mute',
      type: ApplicationCommandOptionType.String,
      required: true,
    },
    {
      name: 'reason',
      description: 'Reason of the mute',
      type: ApplicationCommandOptionType.String,
      required: true,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');
    const duration = interaction.options.getString('duration');
    const convertedTime = ms(duration);
    const reason = interaction.options.getString('reason');

    if (!target.moderatable) return interaction.reply('This member cannot be mute by the bot!');

    if (!convertedTime) return interaction.reply('Specify a correct duration!');

    target.timeout(convertedTime, reason);
    interaction.reply(`The member ${target} was mute for ${duration} because ${reason}!`);
  }
}