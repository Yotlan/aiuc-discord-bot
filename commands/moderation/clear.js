const { ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'clear',
  category: 'moderation',
  permissions: [PermissionsBitField.Flags.ManageMessages],
  ownerOnly: false,
  usage: 'clear [amount] <@target>',
  examples: ['clear 50', 'clear 50 @Yotlan LE CROM'],
  description: 'Delete a specify number of message in a channel or from a specify user!',
  options: [
    {
      name: 'message',
      description: 'Amount of message to delete',
      type: ApplicationCommandOptionType.Number,
      required: true,
    },
    {
      name: 'target',
      description: 'Select a user from delete his message',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const amountToDelete = interaction.options.getNumber('message');
    if (amountToDelete > 100 || amountToDelete < 0) return interaction.reply('Amount must be lower to 100 and upper to 0');
    const target = interaction.options.getMember('target');

    const messagesToDelete = await interaction.channel.messages.fetch();

    if (target) {
      let i = 0;
      const filteredTargetMessages = [];
      (await messagesToDelete).filter(msg => {
        if (msg.author.id == target.id && amountToDelete > i) {
          filteredTargetMessages.push(msg); i++;
        }
      });

      await interaction.channel.bulkDelete(filteredTargetMessages, true).then(messages => {
        interaction.reply(`I delete ${messages.size} messages from user ${target}`);
      });
    } else {
      await interaction.channel.bulkDelete(amountToDelete, true).then(messages => {
        interaction.reply(`I delete ${messages.size} messages on this channel!`);
      });
    }
  }
}