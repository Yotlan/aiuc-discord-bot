const { ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'unmute',
  category: 'moderation',
  permissions: [PermissionsBitField.Flags.ModerateMembers],
  ownerOnly: false,
  usage: 'unmute [@member]',
  examples: ['unmute @Yotlan LE CROM'],
  description: 'Unmute a user',
  options: [
    {
      name: 'target',
      description: 'User to unmute',
      type: ApplicationCommandOptionType.User,
      required: true,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    if (!target.isCommunicationDisabled()) return interaction.reply('This member cannot be unmute by the bot!');

    target.timeout(null);
    interaction.reply(`The member ${target} was unmute!`);
  }
}