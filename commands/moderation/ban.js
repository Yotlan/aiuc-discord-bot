const { ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'ban',
  category: 'moderation',
  permissions: [PermissionsBitField.Flags.BanMembers],
  ownerOnly: false,
  usage: 'ban [@member] [reason]',
  examples: ['ban @Yotlan LE CROM It\'s not cool to be bad!'],
  description: 'Ban a user with reason',
  options: [
    {
      name: 'target',
      description: 'User to ban',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
    {
      name: 'reason',
      description: 'Reason of the ban',
      type: ApplicationCommandOptionType.String,
      required: true,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');
    const reason = interaction.options.getString('reason');

    if (!target.bannable) return interaction.reply('This member cannot be ban by the bot!');

    target.ban({ reason });
    interaction.reply(`The member ${target} was ban because ${reason}!`);
  }
}