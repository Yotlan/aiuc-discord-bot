const { EmbedBuilder, PermissionsBitField } = require('discord.js');
const { ResidentEvilRPGCharacter } = require('../../models');

module.exports = {
  name: 'resident-evil',
  category: 'context',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'Use context menu!',
  examples: ['Use context menu!'],
  type: 2,
  async runInteraction(client, interaction) {
    const member = await interaction.guild.members.fetch(interaction.targetId);

    const residentEvilData = await ResidentEvilRPGCharacter.findOne({ id: interaction.guild.id, member: member.id });

    if (!residentEvilData) return interaction.reply({ content: 'You have no character in Resident Evil RPG' })

    const embed = new EmbedBuilder()
      .setAuthor({ name: `${member.user.tag} (${member.id})` })
      .setColor('#0047AB')
      .setImage(`${residentEvilData['picture']}`)
      .setThumbnail(member.user.displayAvatarURL())
      .setDescription(`**+ Lastname**\n${residentEvilData['lastname']}\n\n**+ Firstname**\n${residentEvilData['firstname']}\n\n**+ Surname**\n${residentEvilData['surname']}\n\n**+ Age**\n${residentEvilData['age']}\n\n**+ Sexe**\n${residentEvilData['sexe']}\n\n**+ Family**\n${residentEvilData['family']}\n\n**+ Nationality**\n${residentEvilData['nationality']}\n\n**+ Sexual Orientation**\n${residentEvilData['sexualorientation']}\n\n**+ Political Party**\n${residentEvilData['politicalparty']}\n\n**+ Religion**\n${residentEvilData['religion']}\n\n**+ History**\n${residentEvilData['history']}\n\n${residentEvilData['other']}`);

    interaction.reply({ embeds: [embed] });
  }
}