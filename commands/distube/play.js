const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'play',
  category: 'distube',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'play [track]',
  examples: ['play resident evil ost', 'play https://www.youtube.com/watch?v=cz6o5NoFysM', 'play https://www.youtube.com/playlist?list=PLxKgOL62ZcRInU4Rwbqn_1o1wbEvHr14E'],
  description: 'Play a song!',
  options: [
    {
      name: 'track',
      description: 'The name/url/playlist you want to play.',
      type: ApplicationCommandOptionType.String,
      required: true,
    }
  ],
  async runInteraction(client, interaction) {

    const track = interaction.options.getString('track');

    await interaction.reply({ content: 'Try to play... Please wait!', fetchReply: true });

    if (!interaction.member.voice.channelId)
      return await interaction.reply({
        content: "❌ You need to be in a voice channel!",
        ephemeral: true,
      });
    if (
      client.voice.channelId &&
      interaction.member.voice.channelId !==
      client.voice.channelId
    )
      return await interaction.reply({
        content:
          "❌ You need to be in the same voice channel as me to do that",
        ephemeral: true,
      });

    client.distube.play(interaction.member.voice.channel, track, {
      member: interaction.member,
      textChannel: interaction.channel,
      interaction
    });

    const playEmbed = new EmbedBuilder()
      .setColor(`#0047AB`)
      .setTitle(
        `🎶 New song(s) Added to queue`
      )
      .addFields(
        { name: 'ADDED TRACK', value: `${track}` }
      );

    return await interaction.editReply({ content: ' ', embeds: [playEmbed], ephemeral: false });
  }
}