const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'tatoo',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'tatoo',
  examples: ['tatoo'],
  description: 'Transform your avatar picture in tatoo meme!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Tatoo().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'tatoo.png' });

    interaction.reply({ files: [attachment] });
  }
}