const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'snyder',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'snyder',
  examples: ['snyder'],
  description: 'Transform your avatar picture in a picture Snyder hold!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Snyder().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'snyder.png' });

    interaction.reply({ files: [attachment] });
  }
}