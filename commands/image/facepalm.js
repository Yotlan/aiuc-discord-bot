const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'facepalm',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'facepalm',
  examples: ['facepalm'],
  description: 'Transform your avatar picture do a facepalm!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Facepalm().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'facepalm.png' });

    interaction.reply({ files: [attachment] });
  }
}