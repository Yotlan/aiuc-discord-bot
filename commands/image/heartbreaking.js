const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'heartbreaking',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'heartbreaking',
  examples: ['heartbreaking'],
  description: 'Add your avatar picture in a heartbreaking!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Heartbreaking().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'heartbreaking.png' });

    interaction.reply({ files: [attachment] });
  }
}