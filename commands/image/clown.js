const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'clown',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'clown',
  examples: ['clown'],
  description: 'Transform your avatar picture in clown meme!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Clown().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'clown.png' });

    interaction.reply({ files: [attachment] });
  }
}