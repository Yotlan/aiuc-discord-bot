const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'blur',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'blur [level]',
  examples: ['blur 1', 'blur 5'],
  description: 'Blur your avatar picture!',
  options: [
    {
      name: 'level',
      description: 'Level of blur',
      type: ApplicationCommandOptionType.Number,
      minValue: 1,
      maxValue: 100,
      required: true,
    },
  ],
  async runInteraction(client, interaction) {
    const level = interaction.options.getNumber('level');

    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Blur().getImage(avatar, level);
    
    const attachment = new AttachmentBuilder(img, { name: 'blur.png' });

    interaction.reply({ files: [attachment] });
  }
}