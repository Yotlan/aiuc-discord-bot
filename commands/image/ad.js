const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'ad',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'ad',
  examples: ['ad'],
  description: 'Transform your avatar picture in Ad!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Ad().getImage(avatar);
    
    const attachment = new AttachmentBuilder(img, { name: 'ad.png' });

    interaction.reply({ files: [attachment] });
  }
}