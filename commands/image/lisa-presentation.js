const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'lisa-presentation',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'lisa-presentation [text]',
  examples: ['lisa-presentation Enter in nightmare!'],
  description: 'Lisa present your text!',
  options: [
    {
      name: 'text',
      description: 'Text you want Lisa to present',
      type: ApplicationCommandOptionType.String,
      required: true,
    },
  ],
  async runInteraction(client, interaction) {
    const text = interaction.options.getString('text');

    if (text.length > 300) return interaction.reply(`You enter a text with a length of ${text.length}. Please enter a text with a length less than ${text.length}`);

    let img = await new DIG.LisaPresentation().getImage(text);

    const attachment = new AttachmentBuilder(img, { name: 'lisa-presentation.png' });

    interaction.reply({ files: [attachment] });
  }
}