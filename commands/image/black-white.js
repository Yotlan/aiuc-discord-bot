const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'black-white',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'black-white',
  examples: ['black-white'],
  description: 'Transform your avatar picture in black and white!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Greyscale().getImage(avatar);
    
    const attachment = new AttachmentBuilder(img, { name: 'black-white.png' });

    interaction.reply({ files: [attachment] });
  }
}