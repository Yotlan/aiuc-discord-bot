const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'rip',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'rip',
  examples: ['rip'],
  description: 'Your avatar picture die!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Rip().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'rip.png' });

    interaction.reply({ files: [attachment] });
  }
}