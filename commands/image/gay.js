const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'gay',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'gay',
  examples: ['gay'],
  description: 'Transform your avatar picture with adding LGBTQ+ color!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Gay().getImage(avatar);
    
    const attachment = new AttachmentBuilder(img, { name: 'gay.png' });

    interaction.reply({ files: [attachment] });
  }
}