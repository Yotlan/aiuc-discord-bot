const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'thomas',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'thomas',
  examples: ['tatoo'],
  description: 'Transform your avatar picture in thomas!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Thomas().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'thomas.png' });

    interaction.reply({ files: [attachment] });
  }
}