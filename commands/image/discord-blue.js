const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'discord-blue',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'discord-blue',
  examples: ['discord-blue'],
  description: 'Transform your avatar picture in discord blue view!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.DiscordBlue().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'discord-blue.png' });

    interaction.reply({ files: [attachment] });
  }
}