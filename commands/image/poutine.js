const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'poutine',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'poutine',
  examples: ['poutine'],
  description: 'Add your avatar picture in Poutine office as a paint!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Poutine().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'poutine.png' });

    interaction.reply({ files: [attachment] });
  }
}