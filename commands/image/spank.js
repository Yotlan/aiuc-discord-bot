const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'spank',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'spank [@member]',
  examples: ['spank @AIUC'],
  description: 'Your avatar picture spank other users avatar picture!',
  options: [
    {
      name: 'user',
      description: 'User you want to spank',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
  ],
  async runInteraction(client, interaction) {
    const user = interaction.options.getUser('user');

    let avatar_1 = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let avatar_2 = user.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Spank().getImage(avatar_1, avatar_2);
    
    const attachment = new AttachmentBuilder(img, { name: 'spank.png' });

    interaction.reply({ files: [attachment] });
  }
}