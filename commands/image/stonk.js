const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'stonk',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'stonk',
  examples: ['stonk'],
  description: 'Transform your avatar picture in stonk meme!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Stonk().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'stonk.png' });

    interaction.reply({ files: [attachment] });
  }
}