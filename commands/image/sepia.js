const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'sepia',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'sepia',
  examples: ['sepia'],
  description: 'Transform your avatar picture in sepia!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Sepia().getImage(avatar);
    
    const attachment = new AttachmentBuilder(img, { name: 'sepia.png' });

    interaction.reply({ files: [attachment] });
  }
}