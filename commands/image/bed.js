const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'bed',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'bed [@member]',
  examples: ['bed @AIUC'],
  description: 'Your avatar picture and other users avatar picture in bed meme!',
  options: [
    {
      name: 'user',
      description: 'User you want to do the bed meme',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
  ],
  async runInteraction(client, interaction) {
    const user = interaction.options.getUser('user');

    let avatar_1 = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let avatar_2 = user.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Bed().getImage(avatar_1, avatar_2);
    
    const attachment = new AttachmentBuilder(img, { name: 'bed.png' });

    interaction.reply({ files: [attachment] });
  }
}