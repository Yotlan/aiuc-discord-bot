const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'wanted',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'wanted [currency]',
  examples: ['wanted $', 'wanted €'],
  description: 'Your avatar picture is wanted!',
  options: [
    {
      name: 'currency',
      description: 'currency you want to use',
      type: ApplicationCommandOptionType.String,
      required: true,
    }
  ],
  async runInteraction(client, interaction) {
    const currency = interaction.options.getString('currency');

    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Wanted().getImage(avatar, currency);
    
    const attachment = new AttachmentBuilder(img, { name: 'wanted.png' });

    interaction.reply({ files: [attachment] });
  }
}