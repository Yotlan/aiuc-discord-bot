const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'bobross',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'bobross',
  examples: ['bobross'],
  description: 'Transform your avatar picture in Bobross paint!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Bobross().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'bobross.png' });

    interaction.reply({ files: [attachment] });
  }
}