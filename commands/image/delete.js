const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'delete',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'delete',
  examples: ['delete'],
  description: 'Transform your avatar picture in delete meme!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Delete().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'delete.png' });

    interaction.reply({ files: [attachment] });
  }
}