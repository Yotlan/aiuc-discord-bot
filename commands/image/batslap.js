const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'batslap',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'batslap [@member]',
  examples: ['batslap @AIUC'],
  description: 'Your avatar picture batslap other users avatar picture!',
  options: [
    {
      name: 'user',
      description: 'User you want to batslap',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
  ],
  async runInteraction(client, interaction) {
    const user = interaction.options.getUser('user');

    let avatar_1 = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let avatar_2 = user.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Batslap().getImage(avatar_1, avatar_2);
    
    const attachment = new AttachmentBuilder(img, { name: 'batslap.png' });

    interaction.reply({ files: [attachment] });
  }
}