const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'blink',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'blink [@member_1] ... <@member_4>',
  examples: ['blink @Yotlan LE CROM', 'blink @Yotlan LE CROM @AIUC'],
  description: 'Blink your avatar picture with other users avatar picture!',
  options: [
    {
      name: 'user_1',
      description: 'User you want to blink with',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
    {
      name: 'user_2',
      description: 'User you want to blink with',
      type: ApplicationCommandOptionType.User,
      required: false,
    },
    {
      name: 'user_3',
      description: 'User you want to blink with',
      type: ApplicationCommandOptionType.User,
      required: false,
    },
    {
      name: 'user_4',
      description: 'User you want to blink with',
      type: ApplicationCommandOptionType.User,
      required: false,
    },
  ],
  async runInteraction(client, interaction) {
    const user_1 = interaction.options.getUser('user_1');
    const user_2 = interaction.options.getUser('user_2');
    const user_3 = interaction.options.getUser('user_3');
    const user_4 = interaction.options.getUser('user_4');

    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let avatar_1 = user_1.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = null;
    
    if (user_2) {
      let avatar_2 = user_2.displayAvatarURL({
        forceStatic: true,
        extension: 'png'
      });

      if (user_3) {
        let avatar_3 = user_3.displayAvatarURL({
          forceStatic: true,
          extension: 'png'
        });

        if (user_4) {
          let avatar_4 = user_4.displayAvatarURL({
            forceStatic: true,
            extension: 'png'
          });

          img = await new DIG.Blink().getImage(2000, avatar, avatar_1, avatar_2, avatar_3, avatar_4);
        } else {
          img = await new DIG.Blink().getImage(2000, avatar, avatar_1, avatar_2, avatar_3);  
        }
      } else {
        img = await new DIG.Blink().getImage(2000, avatar, avatar_1, avatar_2);
      }
    } else {
      img = await new DIG.Blink().getImage(2000, avatar, avatar_1);
    }
    
    const attachment = new AttachmentBuilder(img, { name: 'blink.gif' });

    interaction.reply({ files: [attachment] });
  }
}