const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'mikkelsen',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'mikkelsen',
  examples: ['mikkelsen'],
  description: 'Transform your avatar picture in Mikkelsen meme!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Mikkelsen().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'mikkelsen.png' });

    interaction.reply({ files: [attachment] });
  }
}