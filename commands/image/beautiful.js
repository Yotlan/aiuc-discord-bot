const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'beautiful',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'beautiful',
  examples: ['beautiful'],
  description: 'Transform your avatar picture in Beautiful meme!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Beautiful().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'beautiful.png' });

    interaction.reply({ files: [attachment] });
  }
}