const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'deepfry',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'deepfry',
  examples: ['deepfry'],
  description: 'Transform your avatar picture in deepfry meme!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Deepfry().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'deepfry.png' });

    interaction.reply({ files: [attachment] });
  }
}