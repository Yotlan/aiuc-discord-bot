const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'not-stonk',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'not-stonk',
  examples: ['not-stonk'],
  description: 'Transform your avatar picture in not stonk meme!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.NotStonk().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'not-stonk.png' });

    interaction.reply({ files: [attachment] });
  }
}