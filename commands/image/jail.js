const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'jail',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'jail',
  examples: ['jail'],
  description: 'Add your avatar picture in jail!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Jail().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'jail.png' });

    interaction.reply({ files: [attachment] });
  }
}