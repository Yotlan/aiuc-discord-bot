const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'hitler',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'hitler',
  examples: ['hitler'],
  description: 'Add your avatar picture in tv show about hitler!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Hitler().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'hitler.png' });

    interaction.reply({ files: [attachment] });
  }
}