const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'invert',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'invert',
  examples: ['invert'],
  description: 'Transform your avatar picture in inverted style!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Invert().getImage(avatar);
    
    const attachment = new AttachmentBuilder(img, { name: 'invert.png' });

    interaction.reply({ files: [attachment] });
  }
}