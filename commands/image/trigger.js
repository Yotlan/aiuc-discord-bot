const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'trigger',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'trigger',
  examples: ['trigger'],
  description: 'Trigger your avatar picture!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Triggered().getImage(avatar);
    
    const attachment = new AttachmentBuilder(img, { name: 'triggered.gif' });

    interaction.reply({ files: [attachment] });
  }
}