const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'trash',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'trash',
  examples: ['trash'],
  description: 'Transform your avatar picture in trash meme!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Trash().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'trash.png' });

    interaction.reply({ files: [attachment] });
  }
}