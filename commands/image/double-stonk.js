const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'double-stonk',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'double-stonk [@member]',
  examples: ['double-stonk @AIUC'],
  description: 'Your avatar picture and other users avatar picture double stonk!',
  options: [
    {
      name: 'user',
      description: 'User you want to do double stonk meme',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
  ],
  async runInteraction(client, interaction) {
    const user = interaction.options.getUser('user');

    let avatar_1 = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let avatar_2 = user.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.DoubleStonk().getImage(avatar_1, avatar_2);
    
    const attachment = new AttachmentBuilder(img, { name: 'double-stonk.png' });

    interaction.reply({ files: [attachment] });
  }
}