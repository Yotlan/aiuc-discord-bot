const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'podium',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'podium [@member_1] [@member_2]',
  examples: ['podium @Yotlan LE CROM @AIUC'],
  description: 'Your avatar picture in a podium with other users avatar picture!',
  options: [
    {
      name: 'user_1',
      description: 'User you want to be in the podium',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
    {
      name: 'user_2',
      description: 'User you want to be in the podium',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
  ],
  async runInteraction(client, interaction) {
    const user_1 = interaction.options.getUser('user_1');
    const user_2 = interaction.options.getUser('user_2');

    let avatar_1 = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let avatar_2 = user_1.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let avatar_3 = user_2.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Podium().getImage(avatar_1, avatar_2, avatar_3, interaction.member.user.username, user_1.username, user_2.username);
    
    const attachment = new AttachmentBuilder(img, { name: 'podium.png' });

    interaction.reply({ files: [attachment] });
  }
}