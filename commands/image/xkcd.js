const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const fetch = require('node-fetch');

module.exports = {
  name: 'xkcd',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'xkcd <id>',
  examples: ['xkcd', 'xkcd 56'],
  description: 'Get a random or a specific XKCD comic',
  options: [
    {
      name: 'id',
      description: 'XKCD comic id',
      type: ApplicationCommandOptionType.Number,
      minValue: 1,
      required: false,
    },
  ],
  async runInteraction(client, interaction) {
    const id = interaction.options.getNumber('id');
    const response = await fetch('https://xkcd.com/info.0.json');
    const data = await response.json();
    const max = parseInt(data["num"]);

    var attachment = null;
    
    if (id) {
      if (id > max) return interaction.reply(`Please specify a correct XKCD comic id, less or equal to ${max}!`);
      const xkcd = await fetch(`https://xkcd.com/${id}/info.0.json`);
      const xkcd_comic = await xkcd.json();
      attachment = new AttachmentBuilder(xkcd_comic["img"], { name: 'xkcd-comic.png' });
    } else {
      const xkcd = await fetch(`https://xkcd.com/${Math.floor(Math.random() * max)+1}/info.0.json`);
      const xkcd_comic = await xkcd.json();
      attachment = new AttachmentBuilder(xkcd_comic["img"], { name: 'xkcd-comic.png' });
    }

    interaction.reply({ files: [attachment] });
  }
}