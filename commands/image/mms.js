const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'mms',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'mms',
  examples: ['mms'],
  description: 'Transform your avatar picture in MMS!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Mms().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'mms.png' });

    interaction.reply({ files: [attachment] });
  }
}