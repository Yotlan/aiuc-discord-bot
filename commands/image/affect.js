const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'affect',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'affect',
  examples: ['affect'],
  description: 'Transform your avatar picture in Affect meme!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Affect().getImage(avatar);
    
    const attachment = new AttachmentBuilder(img, { name: 'affect.png' });

    interaction.reply({ files: [attachment] });
  }
}