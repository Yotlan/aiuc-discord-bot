const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'karaba',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'karaba',
  examples: ['karaba'],
  description: 'Transform your avatar picture in Karaba!',
  async runInteraction(client, interaction) {
    let avatar = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Karaba().getImage(avatar);

    const attachment = new AttachmentBuilder(img, { name: 'karaba.png' });

    interaction.reply({ files: [attachment] });
  }
}