const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const DIG = require('discord-image-generation');

module.exports = {
  name: 'kiss',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'kiss [@member]',
  examples: ['kiss @AIUC'],
  description: 'Your avatar picture kiss other users avatar picture!',
  options: [
    {
      name: 'user',
      description: 'User you want to kiss',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
  ],
  async runInteraction(client, interaction) {
    const user = interaction.options.getUser('user');

    let avatar_1 = interaction.member.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let avatar_2 = user.displayAvatarURL({
      forceStatic: true,
      extension: 'png'
    });

    let img = await new DIG.Kiss().getImage(avatar_1, avatar_2);
    
    const attachment = new AttachmentBuilder(img, { name: 'kiss.png' });

    interaction.reply({ files: [attachment] });
  }
}