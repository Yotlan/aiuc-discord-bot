const { AttachmentBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const google = require('googlethis');

module.exports = {
  name: 'search-image',
  category: 'image',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'search-image [query]',
  examples: ['search-image resident evil', 'search resident evil nemesis'],
  description: 'Get a random picture corresponding to your query',
  options: [
    {
      name: 'query',
      description: 'What you want to search',
      type: ApplicationCommandOptionType.String,
      required: true,
    }
  ],
  async runInteraction(client, interaction) {
    const query = interaction.options.getString('query');

    const images = await google.image(query, { safe: false });

    const attachment = new AttachmentBuilder(images[Math.floor(Math.random() * images.length)]["url"], { name: 'search-image.png' });

    interaction.reply({ files: [attachment] });
  }
}