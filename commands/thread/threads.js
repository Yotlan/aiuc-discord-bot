const { ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'thread',
  category: 'thread',
  permissions: [PermissionsBitField.Flags.ManageThreads],
  ownerOnly: false,
  usage: 'thread [join|leave|archive|unarchive|delete]',
  examples: ['thread join', 'thread leave'],
  description: 'Command about threads',
  options: [
    {
      name: 'join',
      description: 'Join a thread',
      type: ApplicationCommandOptionType.Subcommand,
    },
    {
      name: 'leave',
      description: 'Leave a thread',
      type: ApplicationCommandOptionType.Subcommand,
    },
    {
      name: 'archive',
      description: 'Archive a thread',
      type: ApplicationCommandOptionType.Subcommand,
    },
    {
      name: 'unarchive',
      description: 'Unarchive a thread',
      type: ApplicationCommandOptionType.Subcommand,
    },
    {
      name: 'delete',
      description: 'Delete a thread',
      type: ApplicationCommandOptionType.Subcommand
    },
  ],
  async runInteraction(client, interaction) {
    let thread = interaction.channel;
    if (!thread.isThread()) return interaction.reply('Unable to launch this command because you\'re not in a thread!');

    if (interaction.options.getSubcommand() === 'join') {
      interaction.reply('The bot join the thread');
      if (thread.joinable) await thread.join();
    } else if (interaction.options.getSubcommand() === 'leave') {
      interaction.reply('The bot leave the thread');
      await thread.leave();
    } else if (interaction.options.getSubcommand() === 'archive') {
      await interaction.reply('The thread is archive');
      await thread.setArchived(true);
    } else if (interaction.options.getSubcommand() === 'unarchive') {
      interaction.reply('The thread is archive');
      await thread.setArchived(false);
    } else if (interaction.options.getSubcommand() === 'delete') {
      const botLogChannel = await client.getLogChannel(interaction.guild);
      await botLogChannel.send(`The bot delete the thread ${thread.name}`);
      await thread.delete();
    }
  }
}