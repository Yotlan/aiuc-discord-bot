const { ActionRowBuilder, ButtonBuilder, PermissionsBitField } = require('discord.js');

const buttons = new ActionRowBuilder()
  .addComponents(
    new ButtonBuilder()
      .setCustomId('primary-button')
      .setLabel('Primary')
      .setStyle('Primary'),
    new ButtonBuilder()
      .setCustomId('secondary-button')
      .setLabel('Secondary')
      .setStyle('Secondary'),
    new ButtonBuilder()
      .setCustomId('success-button')
      .setLabel('Success')
      .setStyle('Success'),
    new ButtonBuilder()
      .setCustomId('danger-button')
      .setLabel('Danger')
      .setStyle('Danger'),
    new ButtonBuilder()
      .setURL('https://google.com')
      .setLabel('Link')
      .setStyle('Link'),
  )

module.exports = {
  name: 'button',
  category: 'utils',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'button',
  examples: ['button'],
  description: 'Command to see all existing buttons',
  async runInteraction(client, interaction) {
    await interaction.reply({ content: 'Click on buttons', components: [buttons] });
  }
}