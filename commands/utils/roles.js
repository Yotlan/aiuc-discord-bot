const { ActionRowBuilder, StringSelectMenuBuilder, EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'roles',
  category: 'utils',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'roles',
  examples: ['roles'],
  description: 'Select an initial role to begin your journey in different RPG...',
  options: [
    {
      name: 'rpg',
      description: 'Enter the name of RPG you want to have roles',
      type: ApplicationCommandOptionType.String,
      required: true,
      choices: [
        {
          name: 'ResidentEvil',
          value: 'ResidentEvil'
        }
      ]
    }
  ],
  async runInteraction(client, interaction) {
    const rpg = interaction.options.getString('rpg');

    if (rpg === 'ResidentEvil') {
      const criminal = await client.getRole(interaction.guild, 'CRIMINAL');
      const stars = await client.getRole(interaction.guild, 'STARS');

      const rpgEmbed = new EmbedBuilder()
        .setColor('#0047AB')
        .setThumbnail('https://upload.wikimedia.org/wikipedia/commons/0/0e/Umbrella_Corporation_logo.svg')
        .setTimestamp()
        .setTitle(':zombie: Resident Evil')
        .setDescription('Welcome to Resident Evil RPG! Select a role above to begin your journey in this wonderful RPG...')
        .setImage('https://static.hitek.fr/img/actualite/ill_m/1570193469/r6dedatmxxnxvqmsndi7ak.webp')
        .setFooter({ text: 'Welcome to Resident Evil!' })

      const selectMenu = new ActionRowBuilder()
        .addComponents(
          new StringSelectMenuBuilder()
            .setCustomId('roles-menu')
            .setPlaceholder('Choose a role to play!')
            .setMinValues(1)
            .setMaxValues(1)
            .addOptions([
              {
                label: 'CRIMINAL',
                description: 'Choose to play a criminal!',
                value: `${criminal.id}`
              },
              {
                label: 'STARS',
                description: 'Choose to play a STARS!',
                value: `${stars.id}`
              }
            ])
        )

      await interaction.reply({ embeds: [rpgEmbed], components: [selectMenu] });
    }
  }
}