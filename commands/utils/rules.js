const { EmbedBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'rules',
  category: 'utils',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'rules [refuse_behavior] <rule_1> ... <rule_17>',
  examples: ['rules hard first rule second rule', 'rules moderate first rule'],
  description: 'The rules command send an embed message for rule with button to accept or not the rules',
  options: [
    {
      name: 'refuse_behavior',
      description: 'Choose the behavior link with the fact of refuse the rules',
      type: ApplicationCommandOptionType.String,
      required: true,
      choices: [
        {
          name: 'hard',
          description: 'Kick the member who not decide to accept the rules!',
          value: 'hard'
        },
        {
          name: 'moderate',
          description: 'Send to the member who not accept the rules to tell him to accept the rules!',
          value: 'moderate'
        },
        {
          name: 'soft',
          description: 'Do nothing and let the member accept the rules later!',
          value: 'soft'
        }
      ]
    },
    {
      name: 'rule_1',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_2',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_3',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_4',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_5',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_6',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_7',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_8',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_9',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_10',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_11',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_12',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_13',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_14',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_15',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_16',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'rule_17',
      description: 'Enter the content of the rule',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
  ],
  async runInteraction(client, interaction) {
    const accept = interaction.options.getString('accept_behavior');
    const refuse = interaction.options.getString('refuse_behavior');
    const rule1 = interaction.options.getString('rule_1');
    const rule2 = interaction.options.getString('rule_2');
    const rule3 = interaction.options.getString('rule_3');
    const rule4 = interaction.options.getString('rule_4');
    const rule5 = interaction.options.getString('rule_5');
    const rule6 = interaction.options.getString('rule_6');
    const rule7 = interaction.options.getString('rule_7');
    const rule8 = interaction.options.getString('rule_8');
    const rule9 = interaction.options.getString('rule_9');
    const rule10 = interaction.options.getString('rule_10');
    const rule11 = interaction.options.getString('rule_11');
    const rule12 = interaction.options.getString('rule_12');
    const rule13 = interaction.options.getString('rule_13');
    const rule14 = interaction.options.getString('rule_14');
    const rule15 = interaction.options.getString('rule_15');
    const rule16 = interaction.options.getString('rule_16');
    const rule17 = interaction.options.getString('rule_17');

    try {
      const rulesEmbed = new EmbedBuilder()
        .setTitle(':clipboard: Rules of the server')
        .setColor('#0047AB')
        .addFields(
          { name: '+ Bot Rule #1', value: '\`Please do not spam command\`' },
          { name: '+ Bot Rule #2', value: '\`If the bot not respond, please send an email to the main developer : yotlanlecrom@yahoo.fr\`' },
          { name: '+ Bot Rule #3', value: '\`Do not share the bot to anyone without consent of his main developer : Yotlan LE CROM \`' },
          { name: '+ Bot Rule #4', value: '\`This bot is under development, so please add the main developer on your server to let him moderate this bot. To add him, please send an email at : yotlanlecrom@yahoo.fr, and tell him to give you his tag account!\`' }
        )
        .setFooter({ text: 'Welcome on this server!' })
        .setTimestamp()

      if (rule1) rulesEmbed.addFields({ name: '+ Rule #1', value: `${rule1}` });
      if (rule2) rulesEmbed.addFields({ name: '+ Rule #2', value: `${rule2}` });
      if (rule3) rulesEmbed.addFields({ name: '+ Rule #3', value: `${rule3}` });
      if (rule4) rulesEmbed.addFields({ name: '+ Rule #4', value: `${rule4}` });
      if (rule5) rulesEmbed.addFields({ name: '+ Rule #5', value: `${rule5}` });
      if (rule6) rulesEmbed.addFields({ name: '+ Rule #6', value: `${rule6}` });
      if (rule7) rulesEmbed.addFields({ name: '+ Rule #7', value: `${rule7}` });
      if (rule8) rulesEmbed.addFields({ name: '+ Rule #8', value: `${rule8}` });
      if (rule9) rulesEmbed.addFields({ name: '+ Rule #9', value: `${rule9}` });
      if (rule10) rulesEmbed.addFields({ name: '+ Rule #10', value: `${rule10}` });
      if (rule11) rulesEmbed.addFields({ name: '+ Rule #11', value: `${rule11}` });
      if (rule12) rulesEmbed.addFields({ name: '+ Rule #12', value: `${rule12}` });
      if (rule13) rulesEmbed.addFields({ name: '+ Rule #13', value: `${rule13}` });
      if (rule14) rulesEmbed.addFields({ name: '+ Rule #14', value: `${rule14}` });
      if (rule15) rulesEmbed.addFields({ name: '+ Rule #15', value: `${rule15}` });
      if (rule16) rulesEmbed.addFields({ name: '+ Rule #16', value: `${rule16}` });
      if (rule17) rulesEmbed.addFields({ name: '+ Rule #17', value: `${rule17}` });

      if (refuse === 'hard') {
        const buttons = new ActionRowBuilder()
          .addComponents(
            new ButtonBuilder()
              .setCustomId('accept-button')
              .setLabel('Accept')
              .setStyle(ButtonStyle.Success),
            new ButtonBuilder()
              .setCustomId('hard-refuse-button')
              .setLabel('Refuse')
              .setStyle(ButtonStyle.Danger),
          )

        await interaction.reply({ embeds: [rulesEmbed], components: [buttons] });
      } else if (refuse === 'moderate') {
        const buttons = new ActionRowBuilder()
          .addComponents(
            new ButtonBuilder()
              .setCustomId('accept-button')
              .setLabel('Accept')
              .setStyle(ButtonStyle.Success),
            new ButtonBuilder()
              .setCustomId('moderate-refuse-button')
              .setLabel('Refuse')
              .setStyle(ButtonStyle.Danger),
          )

        await interaction.reply({ embeds: [rulesEmbed], components: [buttons] });
      } else {
        const buttons = new ActionRowBuilder()
          .addComponents(
            new ButtonBuilder()
              .setCustomId('accept-button')
              .setLabel('Accept')
              .setStyle(ButtonStyle.Success),
            new ButtonBuilder()
              .setCustomId('soft-refuse-button')
              .setLabel('Refuse')
              .setStyle(ButtonStyle.Danger),
          )

        await interaction.reply({ embeds: [rulesEmbed], components: [buttons] });
      }
    } catch (e) {
      await interaction.reply('Please specify a correct role to link with the fact of accepting the rules');
    }

  }
}