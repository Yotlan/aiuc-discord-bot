const { EmbedBuilder, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'ping',
  category: 'utils',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'ping',
  examples: ['ping'],
  description: 'Command ping send BOT\'s latency and API\'s latency',
  async runInteraction(client, interaction) {
    const tryPong = await interaction.reply({ content: 'Try to pong... Please wait!', fetchReply: true });

    const embed = new EmbedBuilder()
      .setTitle('🏓 Pong!')
      .setColor('#0047AB')
      .setThumbnail(client.user.displayAvatarURL())
      .addFields(
        { name: 'API\'s latency', value: `\`\`\`${client.ws.ping}ms\`\`\``, inline: true },
        { name: 'BOT\' latency', value: `\`\`\`${tryPong.createdTimestamp - interaction.createdTimestamp}ms\`\`\``, inline: true }
      )
      .setTimestamp()
      .setFooter({
        text: interaction.user.username,
        iconURL: interaction.user.displayAvatarURL()
      });

    interaction.editReply({ content: ' ', embeds: [embed] });
  }
}