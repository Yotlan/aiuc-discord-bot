const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'roll',
  category: 'utils',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'roll [face] <number>',
  examples: ['roll D2', 'roll D10 3'],
  description: 'Roll one or more dice with your choosen face!',
  options: [
    {
      name: 'face',
      description: 'Choose the behavior link with the fact of refuse the rules',
      type: ApplicationCommandOptionType.Number,
      required: true,
    },
    {
      name: 'number',
      description: 'Choose the number of dice your will roll',
      type: ApplicationCommandOptionType.Number,
      required: false
    }
  ],
  async runInteraction(client, interaction) {
    const face = interaction.options.getNumber('face');
    const number = interaction.options.getNumber('number');

    if (number) {
      let results = [];
      let sum = 0;
      let nbeven = 0;
      let nbodd = 0;
      let random = Math.floor(Math.random() * face) + 1;
      results.push(random);
      sum += random;
      for (let i = 2; i <= number; i++) {
        random = Math.floor(Math.random() * face) + 1;
        results.push(random);
        sum += random;
        if (random % 2 == 0) {
          nbeven++;
        } else {
          nbodd++;
        }
      }

      const avg = sum / number;

      const median = arr => {
        const mid = Math.floor(arr.length / 2),
          nums = [...arr].sort((a, b) => a - b);
        return arr.length % 2 !== 0 ? nums[mid] : (nums[mid - 1] + nums[mid]) / 2;
      };

      const embed = new EmbedBuilder()
        .setTitle(`🎲 ${number}D${face}`)
        .setColor('#0047AB')
        .setDescription(`**+ Max**\n${Math.max(...results)}\n\n**+ Min**\n${Math.min(...results)}\n\n**+ Average**\n${avg}\n\n**+ Median**\n${median(results)}\n\n**+ Sum**\n${sum}\n\n**+ Even's number**\n${nbeven}\n\n**+ Odd's number**\n${nbodd}\n\n**+ Dices results**\n${results.map(elem => `\`${elem}\``).join(' | ')}`);

      interaction.reply({ embeds: [embed] });
    } else {
      interaction.reply({ content: `🎲 1D${face} : \`${Math.floor(Math.random() * face) + 1}\`` });
    }
  }
}