const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const { readdirSync } = require('fs');
const commandFolder = readdirSync('./commands');
const prefix = '/';

module.exports = {
  name: 'help',
  category: 'utils',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'help <command>',
  examples: ['help', 'help ping', 'help emit'],
  description: 'Send a list of all commands filter by category',
  options: [
    {
      name: 'command',
      description: 'Enter the name of your command',
      type: ApplicationCommandOptionType.String,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const cmdName = interaction.options.getString('command');

    if (!cmdName) {
      const noArgsEmbed = new EmbedBuilder()
        .setColor('#0047AB')
        .setTimestamp()
        .setTitle(':tools: Help')
        .addFields(
          { name: 'List of all commands', value: `List of all catégorie available and all their commands.\nFor more information about a command, enter \`/help <command>\`` }
        )

      for (const category of commandFolder) {
        noArgsEmbed.addFields(
          {
            name: `+ ${category.replace(/(^\w|\s\w)/g, firstLetter => firstLetter.toUpperCase())}`,
            value: `\`${client.commands.filter(cmd => cmd.category == category.toLowerCase()).map(cmd => cmd.name).join(', ')}\``
          }
        );
      }

      return interaction.reply({ embeds: [noArgsEmbed] });
    }

    const cmd = client.commands.get(cmdName);
    if (!cmd) return interaction.reply({ content: 'This command does not exist!', ephemeral: true });

    const argsEmbed = new EmbedBuilder()
      .setColor('#0047AB')
      .setTimestamp()
      .setTitle(`:tools: Help for command : ${cmd.name} ${cmd.ownerOnly ? ':warning: Only for ADMINISTRATOR :warning:' : ''}`)
      .setDescription(`${cmd.description ? cmd.description : contextDescription[`${cmd.name}`]}`)
      .addFields(
        { name: '+ Permissions', value: `\`${cmd.permissions.join(', ')}\`` },
        { name: '+ Usage', value: `\`${prefix}${cmd.usage}\`` },
        { name: '+ Examples', value: `\`${prefix}${cmd.examples.join(` | ${prefix}`)}\`` }
      )
      .setFooter({ text: `${prefix} = prefix used by the bot\n{} = subcommand available | [] = necessary option | <> = optional option\nDo not include these characters -> {}, [] and <> in your commands.` })

    return interaction.reply({ embeds: [argsEmbed] });

  }
}