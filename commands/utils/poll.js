const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'poll',
  category: 'utils',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'poll [question] <answer_a> ... <answer_t>',
  examples: ['poll', 'poll `What time is it?` `9 o\'clock!` `Time to go out!`'],
  description: 'Post your own poll!',
  options: [
    {
      name: 'question',
      description: 'Choose the question of your poll!',
      type: ApplicationCommandOptionType.String,
      required: true,
    },
    {
      name: 'answer_a',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_b',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_c',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_d',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_e',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_f',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_g',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_h',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_i',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_j',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_k',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_l',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_m',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_n',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_o',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_p',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_q',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_r',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_s',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'answer_t',
      description: 'Choose an answer for your poll',
      type: ApplicationCommandOptionType.String,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const pollTitle = interaction.options.getString('question');
    var pollContent = "";
    const answerA = interaction.options.getString('answer_a');
    const answerB = interaction.options.getString('answer_b');
    const answerC = interaction.options.getString('answer_c');
    const answerD = interaction.options.getString('answer_d');
    const answerE = interaction.options.getString('answer_e');
    const answerF = interaction.options.getString('answer_f');
    const answerG = interaction.options.getString('answer_g');
    const answerH = interaction.options.getString('answer_h');
    const answerI = interaction.options.getString('answer_i');
    const answerJ = interaction.options.getString('answer_j');
    const answerK = interaction.options.getString('answer_k');
    const answerL = interaction.options.getString('answer_l');
    const answerM = interaction.options.getString('answer_m');
    const answerN = interaction.options.getString('answer_n');
    const answerO = interaction.options.getString('answer_o');
    const answerP = interaction.options.getString('answer_p');
    const answerQ = interaction.options.getString('answer_q');
    const answerR = interaction.options.getString('answer_r');
    const answerS = interaction.options.getString('answer_s');
    const answerT = interaction.options.getString('answer_t');

    const embed = new EmbedBuilder()
      .setTitle(`:bar_chart: ${pollTitle}`)
      .setColor('#0047AB')
      .setTimestamp()
      .setFooter({ text: `New poll created by ${interaction.user.tag}!` });

    if (answerA) pollContent = `${pollContent}:regional_indicator_a: ${answerA}`;
    if (answerB) pollContent = `${pollContent}\n:regional_indicator_b: ${answerB}`;
    if (answerC) pollContent = `${pollContent}\n:regional_indicator_c: ${answerC}`;
    if (answerD) pollContent = `${pollContent}\n:regional_indicator_d: ${answerD}`;
    if (answerE) pollContent = `${pollContent}\n:regional_indicator_e: ${answerE}`;
    if (answerF) pollContent = `${pollContent}\n:regional_indicator_f: ${answerF}`;
    if (answerG) pollContent = `${pollContent}\n:regional_indicator_g: ${answerG}`;
    if (answerH) pollContent = `${pollContent}\n:regional_indicator_h: ${answerH}`;
    if (answerI) pollContent = `${pollContent}\n:regional_indicator_i: ${answerI}`;
    if (answerJ) pollContent = `${pollContent}\n:regional_indicator_j: ${answerJ}`;
    if (answerK) pollContent = `${pollContent}\n:regional_indicator_k: ${answerK}`;
    if (answerL) pollContent = `${pollContent}\n:regional_indicator_l: ${answerL}`;
    if (answerM) pollContent = `${pollContent}\n:regional_indicator_m: ${answerM}`;
    if (answerN) pollContent = `${pollContent}\n:regional_indicator_n: ${answerN}`;
    if (answerO) pollContent = `${pollContent}\n:regional_indicator_o: ${answerO}`;
    if (answerP) pollContent = `${pollContent}\n:regional_indicator_p: ${answerP}`;
    if (answerQ) pollContent = `${pollContent}\n:regional_indicator_q: ${answerQ}`;
    if (answerR) pollContent = `${pollContent}\n:regional_indicator_r: ${answerR}`;
    if (answerS) pollContent = `${pollContent}\n:regional_indicator_s: ${answerS}`;
    if (answerT) pollContent = `${pollContent}\n:regional_indicator_t: ${answerT}`;
    embed.setDescription(pollContent);

    const poll = await interaction.reply({ embeds: [embed], fetchReply: true });
    if (answerA) poll.react('🇦');
    if (answerB) poll.react('🇧');
    if (answerC) poll.react('🇨');
    if (answerD) poll.react('🇩');
    if (answerE) poll.react('🇪');
    if (answerF) poll.react('🇫');
    if (answerG) poll.react('🇬');
    if (answerH) poll.react('🇭');
    if (answerI) poll.react('🇮');
    if (answerJ) poll.react('🇯');
    if (answerK) poll.react('🇰');
    if (answerL) poll.react('🇱');
    if (answerM) poll.react('🇲');
    if (answerN) poll.react('🇳');
    if (answerO) poll.react('🇴');
    if (answerP) poll.react('🇵');
    if (answerQ) poll.react('🇶');
    if (answerR) poll.react('🇷');
    if (answerS) poll.react('🇸');
    if (answerT) poll.react('🇹');
  }
}