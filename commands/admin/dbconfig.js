const { ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'dbconfig',
  category: 'admin',
  permissions: [PermissionsBitField.Flags.Administrator],
  ownerOnly: true,
  usage: 'dbconfig [key] <value>',
  examples: ['dbconfig', 'dbconfig prefix ?', 'dbconfig prefix'],
  description: 'Configure data from database!',
  options: [
    {
      name: 'key',
      description: 'A key to modify or show',
      type: ApplicationCommandOptionType.String,
      required: true,
      choices: [
        {
          name: 'prefix',
          value: 'prefix'
        },
        {
          name: 'logChannel',
          value: 'logChannel'
        }
      ]
    },
    {
      name: 'value',
      description: 'Choose the new value for your key',
      type: ApplicationCommandOptionType.String,
      required: false
    }
  ],
  async runInteraction(client, interaction, guildSettings) {
    const key = interaction.options.getString('key');
    const value = interaction.options.getString('value');

    if (key == 'prefix') {
      if (value) {
        await client.updateGuild(interaction.guild, { prefix: value })
        return interaction.reply({ content: `New value of the prefix: ${value}` });
      }
      interaction.reply({ content: `Value of the prefix: ${guildSettings.prefix}` });
    } else if (key == 'logChannel') {
      if (value) {
        await client.updateGuild(interaction.guild, { logChannel: value })
        return interaction.reply({ content: `New value of the logChannel: ${value}` });
      }
      interaction.reply({ content: `Value of the logChannel: ${guildSettings.logChannel}` });
    }
  }
}