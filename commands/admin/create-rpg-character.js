const { ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');
const { ResidentEvilRPGCharacter } = require('../../models');
const Logger = require('../../utils/Logger');

module.exports = {
  name: 'create-rpg-character',
  category: 'admin',
  permissions: [PermissionsBitField.Flags.Administrator],
  ownerOnly: false,
  usage: 'create-rpg-character [rpg] <info_1> ... <info_17>',
  examples: ['create-rpg-character residentEvil SILVER Yugere 27'],
  description: 'Add a new rpg\'s character to the database!',
  options: [
    {
      name: 'rpg',
      description: 'The rpg of the new character',
      type: ApplicationCommandOptionType.String,
      required: true,
      choices: [
        {
          name: 'residentEvil',
          value: 'residentEvil'
        }
      ]
    },
    {
      name: 'info_1',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_2',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_3',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_4',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_5',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_6',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_7',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_8',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_9',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_10',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_11',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_12',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_13',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_14',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_15',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_16',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
    {
      name: 'info_17',
      description: 'Enter the content of the information',
      type: ApplicationCommandOptionType.String,
      required: false,
    },
  ],
  async runInteraction(client, interaction, guildSettings) {
    const rpg = interaction.options.getString('rpg');

    if (rpg == 'residentEvil') {
      const id = interaction.guild.id;
      const member = interaction.options.getString('info_1');
      const lastname = interaction.options.getString('info_2');
      const firstname = interaction.options.getString('info_3');
      const surname = interaction.options.getString('info_4');
      const age = interaction.options.getString('info_5');
      const sexe = interaction.options.getString('info_6');
      const family = interaction.options.getString('info_7');
      const nationality = interaction.options.getString('info_8');
      const sexualorientation = interaction.options.getString('info_9');
      const politicalparty = interaction.options.getString('info_10');
      const religion = interaction.options.getString('info_11');
      const history = interaction.options.getString('info_12');
      const other = interaction.options.getString('info_13');
      const picture = interaction.options.getString('info_14');

      if (!lastname) {
        return interaction.reply('Please specify the character lastname!');
      }
      if (!firstname) {
        return interaction.reply('Please specify the character firstname!');
      }
      if (!surname) {
        return interaction.reply('Please specify the character surname!');
      }
      if (!age) {
        return interaction.reply('Please specify the character age!');
      }
      if (!sexe) {
        return interaction.reply('Please specify the character sexe!');
      }
      if (!family) {
        return interaction.reply('Please specify the character family!');
      }
      if (!nationality) {
        return interaction.reply('Please specify the character nationality!');
      }
      if (!sexualorientation) {
        return interaction.reply('Please specify the character sexual orientation!');
      }
      if (!politicalparty) {
        return interaction.reply('Please specify the character political party!');
      }
      if (!religion) {
        return interaction.reply('Please specify the character religion!');
      }
      if (!history) {
        return interaction.reply('Please specify the character history!');
      }
      if (!picture) {
        return interaction.reply('Please specify the character picture!');
      }
      const createCharacter = new ResidentEvilRPGCharacter(
        {
          id: id,
          member: member,
          lastname: lastname,
          firstname: firstname,
          surname: surname,
          age: age,
          sexe: sexe,
          family: family,
          nationality: nationality,
          sexualorientation: sexualorientation,
          politicalparty: politicalparty,
          religion: religion,
          history: history,
          other: other,
          picture: picture
        }
      );
      createCharacter.save().then(g => Logger.client(`New character for member (${member})`));

      interaction.reply({ content: `New character for member (${member})` });
    }
  }
}