const { ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'emit',
  category: 'admin',
  permissions: [PermissionsBitField.Flags.Administrator],
  ownerOnly: true,
  usage: 'emit [eventName]',
  examples: ['emit', 'emit guildCreate'],
  description: 'Emit an choosen event!',
  options: [
    {
      name: 'event',
      description: 'Choose an event to emit',
      type: ApplicationCommandOptionType.String,
      required: true,
      choices: [
        {
          name: 'guildMemberAdd',
          value: 'guildMemberAdd'
        },
        {
          name: 'guildMemberRemove',
          value: 'guildMemberRemove'
        },
        {
          name: 'guildCreate',
          value: 'guildCreate'
        }
      ]
    }
  ],
  runInteraction(client, interaction) {
    const evtChoices = interaction.options.getString('event');

    if (evtChoices == 'guildMemberAdd') {
      client.emit('guildMemberAdd', interaction.member);
      interaction.reply({ content: 'Emit guildMemberAdd event!', ephemeral: true });
    } else if (evtChoices == 'guildCreate') {
      client.emit('guildCreate', interaction.guild);
      interaction.reply({ content: 'Emit guildCreate event!', ephemeral: true });
    } else {
      client.emit('guildMemberRemove', interaction.member);
      interaction.reply({ content: 'Emit guildMemberRemove event!', ephemeral: true });
    }
  }
}