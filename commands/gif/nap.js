const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'nap',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'nap',
  examples: ['nap'],
  description: 'Take a nap!',
  async runInteraction(client, interaction) {
    const hugList = await client.getRandomGif("sleep anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)
      .setDescription(`${interaction.member} take a nap!`);

    interaction.reply({ embeds: [embed] });
  }
}