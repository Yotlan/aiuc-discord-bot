const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'punch',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'punch <@member>',
  examples: ['punch', 'punch @Yotlan LE CROM'],
  description: 'punch yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'User to punch',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("punch anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} punched ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got punched`);
    }

    interaction.reply({ embeds: [embed] });
  }
}