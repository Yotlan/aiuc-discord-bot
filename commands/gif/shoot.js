const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'shoot',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'shoot <@member>',
  examples: ['shoot', 'shoot @Yotlan LE CROM'],
  description: 'shoot yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'User to shoot',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("shoot anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} shooted ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got shooted`);
    }

    interaction.reply({ embeds: [embed] });
  }
}