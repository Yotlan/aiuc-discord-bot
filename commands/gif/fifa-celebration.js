const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'fifa-celebration',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'fifa-celebration <@member>',
  examples: ['fifa-celebration', 'fifa-celebration @Yotlan LE CROM'],
  description: 'celebrate a goal alone or with someone!',
  options: [
    {
      name: 'target',
      description: 'User to celebrate a goal with',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("fifa celebration", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} celebrate a goal with ${target}`);
    } else {
      embed.setDescription(`${interaction.member} celebrate a goal`);
    }

    interaction.reply({ embeds: [embed] });
  }
}