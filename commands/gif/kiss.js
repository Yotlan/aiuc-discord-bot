const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'kiss',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'kiss <@member>',
  examples: ['kiss', 'kiss @Yotlan LE CROM'],
  description: 'kiss yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'User to kiss',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("kiss anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} kissed ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got kissed`);
    }

    interaction.reply({ embeds: [embed] });
  }
}