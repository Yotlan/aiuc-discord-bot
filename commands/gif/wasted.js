const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'wasted',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'wasted <@member>',
  examples: ['wasted', 'wasted @Yotlan LE CROM'],
  description: 'wasted yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'User to wasted',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("wastedgta", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} wasted ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got wasted`);
    }

    interaction.reply({ embeds: [embed] });
  }
}