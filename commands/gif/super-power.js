const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'super-power',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'super-power',
  examples: ['super-power'],
  description: 'Show your super power!',
  async runInteraction(client, interaction) {
    const hugList = await client.getRandomGif("powerup anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)
      .setDescription(`${interaction.member} discovered his super power!`);

    interaction.reply({ embeds: [embed] });
  }
}