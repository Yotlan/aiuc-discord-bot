const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'high-five',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'high-five <@member>',
  examples: ['high-five', 'high-five @Yotlan LE CROM'],
  description: 'high-five yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'User to high-five',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("highfive anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} high-fived ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got high-fived`);
    }

    interaction.reply({ embeds: [embed] });
  }
}