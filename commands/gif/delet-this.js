const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'delet-this',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'delet-this <@member>',
  examples: ['delet-this', 'delet-this @Yotlan LE CROM'],
  description: 'tell yourself or someone to delet this!',
  options: [
    {
      name: 'target',
      description: 'User to tell to delet this',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("delete anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} tell ${target} to delet this`);
    } else {
      embed.setDescription(`${interaction.member} delet this`);
    }

    interaction.reply({ embeds: [embed] });
  }
}