const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'greet',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'greet <@member>',
  examples: ['greet', 'greet @Yotlan LE CROM'],
  description: 'greet yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'User to greet',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("greet anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} greeted ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got greeted`);
    }

    interaction.reply({ embeds: [embed] });
  }
}