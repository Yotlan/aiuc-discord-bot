const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'hug',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'hug <@member>',
  examples: ['hug', 'hug @Yotlan LE CROM'],
  description: 'Hug yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'User to hug',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("hug anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} hugged ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got hugged`);
    }

    interaction.reply({ embeds: [embed] });
  }
}