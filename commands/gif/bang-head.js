const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'bang-head',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'bang-head',
  examples: ['bang-head'],
  description: 'Bang Head!',
  async runInteraction(client, interaction) {
    const hugList = await client.getRandomGif("banghead anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)
      .setDescription(`${interaction.member} bang head!`);

    interaction.reply({ embeds: [embed] });
  }
}