const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'laugh',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'laugh <@member>',
  examples: ['laugh', 'laugh @Yotlan LE CROM'],
  description: 'laugh on yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'User to laugh on',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("laugh anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} got laughed on ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got laughed`);
    }

    interaction.reply({ embeds: [embed] });
  }
}