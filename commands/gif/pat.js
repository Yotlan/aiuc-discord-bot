const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'pat',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'pat <@member>',
  examples: ['pat', 'pat @Yotlan LE CROM'],
  description: 'Pat yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'User to pat',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("pat anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} pat ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got pat`);
    }

    interaction.reply({ embeds: [embed] });
  }
}