const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'baka',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'baka <@member>',
  examples: ['baka', 'baka @Yotlan LE CROM'],
  description: 'tell yourself or someone you\'re a baka!',
  options: [
    {
      name: 'target',
      description: 'User to tell is a baka',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("baka anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} tell ${target} are a baka!`);
    } else {
      embed.setDescription(`${interaction.member} you're a baka!`);
    }

    interaction.reply({ embeds: [embed] });
  }
}