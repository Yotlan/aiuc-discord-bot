const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'run-away',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'run-away <@member>',
  examples: ['run-away', 'run-away @Yotlan LE CROM'],
  description: 'run-away from something or someone!',
  options: [
    {
      name: 'target',
      description: 'User to run away',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("running away anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} run away from ${target}`);
    } else {
      embed.setDescription(`${interaction.member} run away!`);
    }

    interaction.reply({ embeds: [embed] });
  }
}