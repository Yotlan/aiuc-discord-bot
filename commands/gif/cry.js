const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'cry',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'cry <@member>',
  examples: ['cry', 'cry @Yotlan LE CROM'],
  description: 'cry alone or on someone shoulders!',
  options: [
    {
      name: 'target',
      description: 'User shoulder',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("cry anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} cry on your shoulder ${target}`);
    } else {
      embed.setDescription(`${interaction.member} alone`);
    }

    interaction.reply({ embeds: [embed] });
  }
}