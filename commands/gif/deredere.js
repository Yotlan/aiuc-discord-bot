const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'deredere',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'deredere',
  examples: ['deredere'],
  description: 'Deredere!',
  async runInteraction(client, interaction) {
    const hugList = await client.getRandomGif("deredere anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)
      .setDescription(`${interaction.member} deredere!`);

    interaction.reply({ embeds: [embed] });
  }
}