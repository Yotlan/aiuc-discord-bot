const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'bite',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'bite <@member>',
  examples: ['bite', 'bite @Yotlan LE CROM'],
  description: 'Bite yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'User to bite',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("bite anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} bit ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got bitten`);
    }

    interaction.reply({ embeds: [embed] });
  }
}