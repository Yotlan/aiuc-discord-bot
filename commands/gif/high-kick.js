const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'high-kick',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'high-kick <@member>',
  examples: ['high-kick', 'high-kick @Yotlan LE CROM'],
  description: 'high-kick yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'User to high-kick',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("leg high-kick anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} high-kicked ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got high-kicked`);
    }

    interaction.reply({ embeds: [embed] });
  }
}