const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'scared',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'scared <@member>',
  examples: ['scared', 'scared @Yotlan LE CROM'],
  description: 'scared from something or someone!',
  options: [
    {
      name: 'target',
      description: 'User to get scared',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("scared anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} got scared of ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got scared!`);
    }

    interaction.reply({ embeds: [embed] });
  }
}