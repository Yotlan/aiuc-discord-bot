const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'jojo-pose',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'jojo-pose',
  examples: ['jojo-pose'],
  description: 'Do some Jojo pose!',
  async runInteraction(client, interaction) {
    const hugList = await client.getRandomGif("jojopose jjba", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)
      .setDescription(`${interaction.member} do a Jojo pose!`);

    interaction.reply({ embeds: [embed] });
  }
}