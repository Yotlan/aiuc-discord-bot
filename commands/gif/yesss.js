const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'yesss',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'yesss',
  examples: ['yesss'],
  description: 'Express your success!',
  async runInteraction(client, interaction) {
    const hugList = await client.getRandomGif("yay anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)
      .setDescription(`${interaction.member} has succeeded!`);

    interaction.reply({ embeds: [embed] });
  }
}