const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'cook',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'cook <@member>',
  examples: ['cook', 'cook @Yotlan LE CROM'],
  description: 'cook for yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'Cooking for a user',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("cooking anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} cooking for ${target}`);
    } else {
      embed.setDescription(`${interaction.member} cook for himself`);
    }

    interaction.reply({ embeds: [embed] });
  }
}