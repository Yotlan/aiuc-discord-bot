const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'dab',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'dab',
  examples: ['dab'],
  description: 'Dab!',
  async runInteraction(client, interaction) {
    const hugList = await client.getRandomGif("dab anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)
      .setDescription(`${interaction.member} dab!`);

    interaction.reply({ embeds: [embed] });
  }
}