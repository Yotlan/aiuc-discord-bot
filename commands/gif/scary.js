const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'scary',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'scary',
  examples: ['scary'],
  description: 'Be scary!',
  async runInteraction(client, interaction) {
    const hugList = await client.getRandomGif("creepyface anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)
      .setDescription(`${interaction.member} is scary!`);

    interaction.reply({ embeds: [embed] });
  }
}