const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'happy',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'happy',
  examples: ['happy'],
  description: 'Express your hapiness!',
  async runInteraction(client, interaction) {
    const hugList = await client.getRandomGif("happy anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)
      .setDescription(`${interaction.member} is happy!`);

    interaction.reply({ embeds: [embed] });
  }
}