const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'slash',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'slash <@member>',
  examples: ['slash', 'slash @Yotlan LE CROM'],
  description: 'slash yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'User to slash',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("sword slash anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} slashed ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got slashed`);
    }

    interaction.reply({ embeds: [embed] });
  }
}