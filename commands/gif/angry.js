const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'angry',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'angry <@member>',
  examples: ['angry', 'angry @Yotlan LE CROM'],
  description: 'angry at yourself or someone!',
  options: [
    {
      name: 'target',
      description: 'User to get angry',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("angry anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} got angry at ${target}`);
    } else {
      embed.setDescription(`${interaction.member} got angry`);
    }

    interaction.reply({ embeds: [embed] });
  }
}