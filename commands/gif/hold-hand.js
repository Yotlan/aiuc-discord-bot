const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'hold-hand',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'hold-hand <@member>',
  examples: ['hold-hand', 'hold-hand @Yotlan LE CROM'],
  description: 'hold yourself or someone hand!',
  options: [
    {
      name: 'target',
      description: 'User hand to hold',
      type: ApplicationCommandOptionType.User,
      required: false,
    }
  ],
  async runInteraction(client, interaction) {
    const target = interaction.options.getMember('target');

    const hugList = await client.getRandomGif("handholding anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)

    if (target) {
      embed.setDescription(`${interaction.member} hold ${target} hand`);
    } else {
      embed.setDescription(`${interaction.member} hold my hand`);
    }

    interaction.reply({ embeds: [embed] });
  }
}