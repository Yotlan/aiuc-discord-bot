const { EmbedBuilder, ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

module.exports = {
  name: 'initial-d',
  category: 'gif',
  permissions: [PermissionsBitField.Flags.SendMessages],
  ownerOnly: false,
  usage: 'initial-d',
  examples: ['initial-d'],
  description: 'Do some drift!',
  async runInteraction(client, interaction) {
    const hugList = await client.getRandomGif("initiald drift anime", 200);

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setImage(`${hugList["results"][Math.floor(Math.random() * hugList["results"].length)]["media"][0]["gif"]["url"]}`)
      .setDescription(`${interaction.member} drift!!!`);

    interaction.reply({ embeds: [embed] });
  }
}