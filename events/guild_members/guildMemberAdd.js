const { EmbedBuilder } = require('discord.js');

module.exports = {
  name: 'guildMemberAdd',
  once: false,
  async execute(client, member) {

    const embed = new EmbedBuilder()
      .setAuthor({ name: `${member.user.tag} (${member.id})`, iconURL: member.user.displayAvatarURL() })
      .setColor('#21ff81')
      .setDescription(`± Username: ${member}
      ± Create at: <t:${parseInt(member.user.createdTimestamp / 1000)}:f> (<t:${parseInt(member.user.createdTimestamp / 1000)}:R>)
      ± Join at: <t:${parseInt(member.joinedTimestamp / 1000)}:f> (<t:${parseInt(member.joinedTimestamp / 1000)}:R>)
      `)
      .setTimestamp()
      .setFooter({ text: 'User join!' });

    const botLogChannel = await client.getLogChannel(member.guild);
    botLogChannel.send({ embeds: [embed] });
  }
}