const { ActionRowBuilder, EmbedBuilder, ButtonBuilder } = require('discord.js');

module.exports = {
  name: 'playSong',
  once: false,
  async execute(client, queue, song) {

    const row1 = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('back')
          .setEmoji("⏮️")
          .setStyle('Primary'),
        new ButtonBuilder()
          .setCustomId('pause')
          .setEmoji("⏸️")
          .setStyle('Success'),
        new ButtonBuilder()
          .setCustomId('resume')
          .setEmoji("▶️")
          .setStyle('Success'),
        new ButtonBuilder()
          .setCustomId('shuffle')
          .setEmoji("🔀")
          .setStyle('Success'),
        new ButtonBuilder()
          .setCustomId('skip')
          .setEmoji("⏭️")
          .setStyle('Primary'),
      );

    const row2 = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('mute')
          .setEmoji("🔇")
          .setStyle('Danger'),
        new ButtonBuilder()
          .setCustomId('volume-down')
          .setEmoji("🔉")
          .setStyle('Success'),
        new ButtonBuilder()
          .setCustomId('volume-up')
          .setEmoji("🔊")
          .setStyle('Success'),
        new ButtonBuilder()
          .setCustomId('loop')
          .setEmoji("🔁")
          .setStyle('Success'),
        new ButtonBuilder()
          .setCustomId('stop')
          .setEmoji("⏹️")
          .setStyle('Danger'),
      );

    const embed = new EmbedBuilder()
      .setColor('#0047AB')
      .setTimestamp()
      .setTitle(':notes: Now Playing')
      .setDescription(`**${song.name}**`)
      .addFields(
        { name: 'DURATION', value: `${song.formattedDuration}` },
        { name: 'REQUESTER', value: `${song.user}` },
        { name: 'VIEWS', value: `${song.views}` },
        { name: 'URL', value: `**[Click Here](${song.url})**` },
        { name: 'ARTIST', value: `${song.source}` }
      )

    queue.textChannel.send({ embeds: [embed], components: [row1, row2] });
  }
}