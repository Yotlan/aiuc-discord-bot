module.exports = {
  name: 'threadCreate',
  once: false,
  async execute(client, thread) {
    if (thread.isText()) thread.join();
    const botLogChannel = await client.getLogChannel(thread.guild);
    botLogChannel.send(`Thread\'s name: ${thread.name}`);
  }
}