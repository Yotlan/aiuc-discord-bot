const Logger = require('../../utils/Logger');

module.exports = {
  name: 'ready',
  once: true,
  async execute(client) {
    Logger.client('BOT is ready!');

    const devGuild = await client.guilds.cache.get('1069355488258560122');
    devGuild.commands.set(client.commands.map(cmd => cmd));
  }
}