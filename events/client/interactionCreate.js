const { ApplicationCommandOptionType, PermissionsBitField } = require('discord.js');

const ownerId = '620909442103509033';

module.exports = {
  name: 'interactionCreate',
  once: false,
  async execute(client, interaction) {

    let guildSettings = await client.getGuild(interaction.guild);

    if (!guildSettings) {
      await client.createGuild(interaction.guild);
      guildSettings = await client.getGuild(interaction.guild);
    }

    if (interaction.isCommand() || interaction.isContextMenuCommand()) {
      const cmd = client.commands.get(interaction.commandName);
      if (!cmd) return interaction.reply('This command not exist!');

      if (cmd.ownerOnly) {
        if (interaction.user.id != ownerId) return interaction.reply('The only author to execute this command is the BOT\'s owner!');
      }
      if (!interaction.member.permissions.has([cmd.permissions])) return interaction.reply({ content: `You not have the necessary permissions (\`${cmd.permissions.join(', ')}\`) to launch this command!`, ephemeral: true });

      cmd.runInteraction(client, interaction, guildSettings);
    } else if (interaction.isButton()) {
      const btn = client.buttons.get(interaction.customId);
      if (!btn) return interaction.reply('This button not exist!');

      btn.runInteraction(client, interaction, guildSettings);
    } else if (interaction.isSelectMenu()) {
      const selectMenu = client.selects.get(interaction.customId);
      if (!selectMenu) return interaction.reply('This select menu not exist!');

      selectMenu.runInteraction(client, interaction, guildSettings);
    }
  }
}