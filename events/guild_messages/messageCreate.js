const prefix = '!';
const ownerId = '620909442103509033';

module.exports = {
  name: 'messageCreate',
  once: false,
  async execute(client, message) {
    if (message.author.bot) return;
    if (!message.content.startsWith(prefix)) return;

    let guildSettings = await client.getGuild(message.guild);

    if (!guildSettings) {
      await client.createGuild(message.guild);
      guildSettings = await client.getGuild(message.guild);
    }

    const args = message.content.slice(prefix.length).trim().split(/ +/g);
    const cmdName = args.shift().toLowerCase();
    if (cmdName.length == 0) return;

    let cmd = client.commands.get(cmdName);
    if (!cmd) return message.reply('This command not exist!');

    if (cmd.ownerOnly) {
      if (message.author.id != ownerId) return message.reply('The only author to execute this command is the BOT\'s owner!');
    }

    if (!message.member.permissions.has([cmd.permissions])) return message.reply(`You not have the necessary permissions (\`${cmd.permissions.join(', ')}\`) to launch this command!`);

    if (cmd) cmd.run(client, message, args, guildSettings);
  }
}